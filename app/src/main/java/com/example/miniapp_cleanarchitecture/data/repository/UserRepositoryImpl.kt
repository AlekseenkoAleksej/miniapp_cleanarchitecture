package com.example.miniapp_cleanarchitecture.data.repository

import android.content.Context
import com.example.miniapp_cleanarchitecture.domain.repository.UserRepository
import com.example.miniapp_cleanarchitecture.domain.usecase.models.SaveUserNameParam
import com.example.miniapp_cleanarchitecture.domain.usecase.models.UserName

private const val SHARED_PREFS_NAME = "shared_prefs_name"
private const val KEY_FIRST_NAME = "key_first_name"
private const val KEY_LAST_NAME = "key_last_name"

class UserRepositoryImpl(context: Context): UserRepository {

    val sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)

    override fun saveName(saveParam: SaveUserNameParam): Boolean {
        sharedPreferences.edit().putString(KEY_FIRST_NAME, saveParam.userName).apply()
        return true
    }

    override fun getName(): UserName {

        val firstName = sharedPreferences.getString(KEY_FIRST_NAME, "") ?: ""
        val lastName = sharedPreferences.getString(KEY_LAST_NAME, "") ?: ""

        return UserName(firstName = firstName, lastName = lastName)
    }

}