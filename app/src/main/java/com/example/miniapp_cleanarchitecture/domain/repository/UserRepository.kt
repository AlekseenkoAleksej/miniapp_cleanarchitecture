package com.example.miniapp_cleanarchitecture.domain.repository

import com.example.miniapp_cleanarchitecture.domain.usecase.models.SaveUserNameParam
import com.example.miniapp_cleanarchitecture.domain.usecase.models.UserName

interface UserRepository {

    fun saveName(saveParam: SaveUserNameParam): Boolean

    fun getName(): UserName

}