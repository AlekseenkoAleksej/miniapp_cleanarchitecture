package com.example.miniapp_cleanarchitecture.domain.usecase

import com.example.miniapp_cleanarchitecture.domain.repository.UserRepository
import com.example.miniapp_cleanarchitecture.domain.usecase.models.UserName

class GetUserNameUseCase(private val userRepository: UserRepository) {

    fun execute(): UserName {
        return userRepository.getName()

    }

}