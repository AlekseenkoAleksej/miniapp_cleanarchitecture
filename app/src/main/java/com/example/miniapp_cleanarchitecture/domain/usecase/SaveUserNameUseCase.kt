package com.example.miniapp_cleanarchitecture.domain.usecase

import com.example.miniapp_cleanarchitecture.domain.repository.UserRepository
import com.example.miniapp_cleanarchitecture.domain.usecase.models.SaveUserNameParam

class SaveUserNameUseCase(private val userRepository: UserRepository) {

    fun execute(param: SaveUserNameParam): Boolean {
        val result: Boolean = userRepository.saveName(saveParam = param)
        return result

    }

}