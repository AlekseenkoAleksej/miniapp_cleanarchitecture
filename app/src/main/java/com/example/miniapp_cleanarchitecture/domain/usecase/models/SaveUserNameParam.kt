package com.example.miniapp_cleanarchitecture.domain.usecase.models

class SaveUserNameParam (val userName: String)