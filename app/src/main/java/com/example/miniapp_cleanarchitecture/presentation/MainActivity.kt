package com.example.miniapp_cleanarchitecture.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.miniapp_cleanarchitecture.data.repository.UserRepositoryImpl
import com.example.miniapp_cleanarchitecture.databinding.ActivityMainBinding
import com.example.miniapp_cleanarchitecture.domain.usecase.GetUserNameUseCase
import com.example.miniapp_cleanarchitecture.domain.usecase.SaveUserNameUseCase
import com.example.miniapp_cleanarchitecture.domain.usecase.models.SaveUserNameParam
import com.example.miniapp_cleanarchitecture.domain.usecase.models.UserName

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    private val userRepository by lazy (LazyThreadSafetyMode.NONE) { UserRepositoryImpl(context = applicationContext) }
    private val getUserNameUseCase by lazy (LazyThreadSafetyMode.NONE) { GetUserNameUseCase(userRepository = userRepository) }
    private val saveUserNameUseCase by lazy (LazyThreadSafetyMode.NONE) { SaveUserNameUseCase(userRepository = userRepository) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)


        binding.sendButton.setOnClickListener {

            val text = binding.dataEditText.text.toString()
            val params = SaveUserNameParam(userName = text)
            val result: Boolean = saveUserNameUseCase.execute(param = params)
            binding.dataTextView.text = "Save result $result"

        }

        binding.receiveButton.setOnClickListener {
            val userName: UserName = getUserNameUseCase.execute()
            binding.dataTextView.text = "${userName.firstName} ${userName.lastName}"

        }

    }
}